Deploying Java Applications with the Heroku Maven Plugin
---
mvn clean heroku:deploy

[Open App](https://quiet-escarpment-35253.herokuapp.com/events)

Heroku buildpack
---
https://github.com/pyronlaboratory/heroku-integrated-firefox-geckodriver
