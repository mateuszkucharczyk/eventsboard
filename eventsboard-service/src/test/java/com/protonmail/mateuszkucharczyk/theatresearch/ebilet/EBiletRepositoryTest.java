package com.protonmail.mateuszkucharczyk.theatresearch.ebilet;

import static com.protonmail.mateuszkucharczyk.theatresearch.TestData.aCriteria;
import static com.protonmail.mateuszkucharczyk.theatresearch.TestData.aFriday;
import static com.protonmail.mateuszkucharczyk.theatresearch.TestData.aMonday;
import static com.protonmail.mateuszkucharczyk.theatresearch.TestData.aSunday;
import static com.protonmail.mateuszkucharczyk.theatresearch.TestData.anEventOccursAt;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.google.common.collect.Range;
import com.protonmail.mateuszkucharczyk.theatresearch.Event;
import java.net.URI;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class EBiletRepositoryTest {

  @Test
  @DisplayName("GIVEN an event that will happen on Monday, Friday and Sunday"
      + " WHEN user searches for events between Monday and Friday"
      + " THEN return two events: Monday's event and Friday's event")
  void whenSearchesOverTimePeriodThenShouldFilterEventsByDate() {
    List<Event> events = new EBiletRepository(aSessionFactory()).findBy(
        aCriteria()
            .dates(singletonList(Range.closed(aMonday(), aFriday())))
            .build());

    assertAll(
        () -> assertEquals(2, events.size(), "Events should be filtered by date"),
        () -> assertEquals("MONDAY's event", events.get(0).getTitle()),
        () -> assertEquals("FRIDAY's event", events.get(1).getTitle()));
  }

  @Test
  @DisplayName("GIVEN an event that will happen both on Monday and Friday"
      + " WHEN user searches for events on Monday or on Sunday"
      + " THEN return two events: Monday's event and Friday's event")
  void whenSearchesOverMultipleTimePeriodsThenShouldDeduplicateEvents() {
    List<Event> events = new EBiletRepository(aSessionFactory()).findBy(aCriteria()
            .dates(asList(
                Range.singleton(aMonday()),
                Range.singleton(aFriday()),
                Range.singleton(aSunday())
            ))
            .build());

    assertAll(
        () -> assertEquals(3, events.size(), "Events should be deduplicated"),
        () -> assertEquals("MONDAY's event", events.get(0).getTitle()),
        () -> assertEquals("FRIDAY's event", events.get(1).getTitle()),
        () -> assertEquals("SUNDAY's event", events.get(2).getTitle()));
  }

  private static EBiletSessionFactory aSessionFactory() {
    EBiletSearchPage searchPage = eBiletSearchPageReturning(
        URI.create("https://example.com/anEventDetails"));

    EBiletEventDetailsPage eventDetailsPage = eBiletEventDetailsPageReturning(asList(
        anEventOccursAt(aMonday()).build(),
        anEventOccursAt(aFriday()).build(),
        anEventOccursAt(aSunday()).build()
    ));

    EBiletSession session = mock(EBiletSession.class);
    when(session.navigateToSearchPage())
        .thenReturn(searchPage);
    when(session.navigateToEventDetailsPage(URI.create("https://example.com/anEventDetails")))
        .thenReturn(eventDetailsPage);

    EBiletSessionFactory factory = mock(EBiletSessionFactory.class);
    when(factory.make()).thenReturn(session);
    return factory;
  }

  private static EBiletSearchPage eBiletSearchPageReturning(URI uriToEventDetails) {
    EBiletSearchPage searchPage = mock(EBiletSearchPage.class);
    when(searchPage.searchBy(any()))
        .thenReturn(searchPage);
    when(searchPage.getLinksToEvents())
        .thenReturn(singletonList(uriToEventDetails));
    return searchPage;
  }

  private static EBiletEventDetailsPage eBiletEventDetailsPageReturning(List<Event> events) {
    EBiletEventDetailsPage eventDetailsPage = mock(EBiletEventDetailsPage.class);
    when(eventDetailsPage.navigateTo(URI.create("https://example.com/anEventDetails")))
        .thenReturn(eventDetailsPage);
    when(eventDetailsPage.getEvents())
        .thenReturn(events);
    return eventDetailsPage;
  }
}