package com.protonmail.mateuszkucharczyk.theatresearch;

import static com.protonmail.mateuszkucharczyk.theatresearch.EventCriteria.alwaysMatch;
import static java.util.Arrays.asList;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;

import java.time.LocalDateTime;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

class EventsAggregatorTest {

  @Test
  @DisplayName("GIVEN multiple providers, "
      + "WHEN aggregating their results, "
      + "THEN returned events should be sorted by date.")
  public void shouldSortEventsByDate() {
    List<Event> events = givenAggregatorOfMultipleProviders().findBy(alwaysMatch());

    assertAll("Events should be sorted by date",
        () -> assertEquals("1st Event", events.get(0).getTitle()),
        () -> assertEquals("2nd Event", events.get(1).getTitle()),
        () -> assertEquals("3rd Event", events.get(2).getTitle()),
        () -> assertEquals("4th Event", events.get(3).getTitle())
    );
  }

  private static EventsAggregator givenAggregatorOfMultipleProviders() {
    EventsRepository aProvider = aProviderReturning(Event.builder()
            .provider("aProvider")
            .title("4th Event")
            .date(firstOfJanuary2000AtNoonWithMinutes(20))
            .build(),
        Event.builder()
            .provider("aProvider")
            .title("2nd Event")
            .date(firstOfJanuary2000AtNoonWithMinutes(10))
            .build()
    );

    EventsRepository anOtherProvider = aProviderReturning(Event.builder()
            .provider("anOtherProvider")
            .title("3rd Event")
            .date(firstOfJanuary2000AtNoonWithMinutes(15))
            .build(),
        Event.builder()
            .provider("anOtherProvider")
            .title("1st Event")
            .date(firstOfJanuary2000AtNoonWithMinutes(5))
            .build()
    );

    return new EventsAggregator(asList(aProvider, anOtherProvider));
  }

  private static EventsRepository aProviderReturning(Event... events) {
    EventsRepository aProvider = Mockito.mock(EventsRepository.class);
    Mockito.when(aProvider.findBy(any())).thenReturn(asList(events));
    return aProvider;
  }

  private static LocalDateTime firstOfJanuary2000AtNoonWithMinutes(int minute) {
    return LocalDateTime.of(2000, 1, 1, 12, minute);
  }
}