package com.protonmail.mateuszkucharczyk.theatresearch;

import com.protonmail.mateuszkucharczyk.theatresearch.Event.EventBuilder;
import com.protonmail.mateuszkucharczyk.theatresearch.EventCriteria.EventCriteriaBuilder;
import java.net.URI;
import java.time.LocalDateTime;
import lombok.experimental.UtilityClass;

@UtilityClass
public class TestData {

  public static EventCriteriaBuilder aCriteria() {
    return EventCriteria.builder()
        .city("aCity");
  }

  public static EventBuilder anEventOccursAt(LocalDateTime dateTime) {
    return Event.builder()
        .title(dateTime.getDayOfWeek().name() + "'s event")
        .date(dateTime)
        .descriptionUri(URI.create("https://example.com/anEventDetails"))
        .purchaseUri(
            URI.create(String.format("https://example.com/anEventDetails/on%sId", dateTime.getDayOfWeek().name())))
        .city("aCity");
  }

  public static LocalDateTime aMonday() {
    return LocalDateTime.of(2019, 7, 1, 12, 0);
  }

  public static LocalDateTime aFriday() {
    return LocalDateTime.of(2019, 7, 5, 12, 0);
  }

  public static LocalDateTime aSunday() {
    return LocalDateTime.of(2019, 7, 7, 12, 0);
  }
}
