package com.protonmail.mateuszkucharczyk.theatresearch.multiteatr;

import static com.protonmail.mateuszkucharczyk.theatresearch.TestData.aCriteria;
import static com.protonmail.mateuszkucharczyk.theatresearch.TestData.aFriday;
import static com.protonmail.mateuszkucharczyk.theatresearch.TestData.aMonday;
import static com.protonmail.mateuszkucharczyk.theatresearch.TestData.aSunday;
import static com.protonmail.mateuszkucharczyk.theatresearch.TestData.anEventOccursAt;
import static java.util.Arrays.asList;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.google.common.collect.Range;
import com.protonmail.mateuszkucharczyk.theatresearch.Event;
import java.util.List;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

class MultiteatrRepositoryTest {

  @Test
  @DisplayName("GIVEN an event that will happen on Monday, Friday and Sunday"
      + " WHEN user searches for events between Monday and Friday"
      + " THEN return two events: Monday's event and Friday's event")
  void whenSearchesOverTimePeriodThenShouldFilterEventsByDate() {
    List<Event> events = new MultiteatrRepository(aSessionFactoryReturning(
        anEventOccursAt(aMonday()).build(),
        anEventOccursAt(aFriday()).build(),
        anEventOccursAt(aSunday()).build()
    )).findBy(aCriteria().dates(singletonList(
        Range.closed(aMonday(), aFriday())
    )).build());

    assertAll(
        () -> assertEquals(2, events.size(), "Events should be filtered by date"),
        () -> assertEquals("MONDAY's event", events.get(0).getTitle()),
        () -> assertEquals("FRIDAY's event", events.get(1).getTitle()));
  }

  @Test
  @DisplayName("GIVEN an event that will happen both on Monday and Friday"
      + " WHEN user searches for events on Monday or on Sunday"
      + " THEN return two events: Monday's event and Friday's event")
  void whenSearchesOverMultipleTimePeriodsThenShouldDeduplicateEvents() {
    List<Event> events = new MultiteatrRepository(aSessionFactoryReturning(
        anEventOccursAt(aMonday()).build(),
        anEventOccursAt(aFriday()).build(),
        anEventOccursAt(aSunday()).build()
    )).findBy(aCriteria().dates(asList(
        Range.singleton(aMonday()),
        Range.singleton(aFriday()),
        Range.singleton(aSunday())
    )).build());

    assertAll(
        () -> assertEquals(3, events.size(), "Events should be deduplicated"),
        () -> assertEquals("MONDAY's event", events.get(0).getTitle()),
        () -> assertEquals("FRIDAY's event", events.get(1).getTitle()),
        () -> assertEquals("SUNDAY's event", events.get(2).getTitle()));
  }

  private MultiteatrSessionFactory aSessionFactoryReturning(Event... events) {
    MultiteatrSearchPage searchPage = multiteatrSearchPageReturning(asList(events));

    MultiteatrSession session = mock(MultiteatrSession.class);
    when(session.navigateToSearchPage())
        .thenReturn(searchPage);

    MultiteatrSessionFactory factory = mock(MultiteatrSessionFactory.class);
    when(factory.make()).thenReturn(session);
    return factory;
  }

  private static MultiteatrSearchPage multiteatrSearchPageReturning(List<Event> events) {
    MultiteatrSearchPage searchPage = mock(MultiteatrSearchPage.class);
    when(searchPage.searchBy(any()))
        .thenReturn(searchPage);
    when(searchPage.getSpectacles())
        .thenReturn(events);
    return searchPage;
  }
}