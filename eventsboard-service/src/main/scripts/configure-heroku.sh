#!/usr/bin/env bash

set -o errexit
set -o pipefail
set -o nounset
# using -e without -E will cause an ERR trap to not fire in certain scenarios
set -E

function main() {
  local -r application="${1:?[ERROR] application name not provided}"
  heroku buildpacks:add https://github.com/pyronlaboratory/heroku-integrated-firefox-geckodriver --app "${application}";
  heroku config:set FIREFOX_BIN=/app/vendor/firefox/firefox --app "${application}";
  heroku config:set GECKODRIVER_PATH=/app/vendor/geckodriver/geckodriver --app "${application}";
  heroku config:set LD_LIBRARY_PATH=/usr/local/lib:/usr/lib:/lib:/app/vendor --app "${application}";
  heroku config:set PATH=/usr/local/bin:/usr/bin:/bin:/app/vendor/:/app/vendor/firefox/firefox --app "${application}";
}

main "$@";
