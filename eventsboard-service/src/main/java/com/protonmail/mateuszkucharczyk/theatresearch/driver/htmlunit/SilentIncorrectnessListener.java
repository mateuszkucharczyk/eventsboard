package com.protonmail.mateuszkucharczyk.theatresearch.driver.htmlunit;

import com.gargoylesoftware.htmlunit.IncorrectnessListener;

class SilentIncorrectnessListener implements IncorrectnessListener {

  @Override
  public void notify(String s, Object o) {

  }
}
