package com.protonmail.mateuszkucharczyk.theatresearch.multiteatr;

import static java.util.stream.Collectors.toList;

import com.protonmail.mateuszkucharczyk.theatresearch.Event;
import java.net.URI;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

class MultiteatrEventsMapper {

  private static final String MULTITEATR = "Multiteatr";
  private static final Map<String, Month> months = new HashMap<>();

  static {
    MultiteatrEventsMapper.months.put("stycznia", Month.JANUARY);
    MultiteatrEventsMapper.months.put("lutego", Month.FEBRUARY);
    MultiteatrEventsMapper.months.put("marca", Month.MARCH);
    MultiteatrEventsMapper.months.put("kwietnia", Month.APRIL);
    MultiteatrEventsMapper.months.put("maja", Month.MAY);
    MultiteatrEventsMapper.months.put("czerwca", Month.JUNE);
    MultiteatrEventsMapper.months.put("lipca", Month.JULY);
    MultiteatrEventsMapper.months.put("sierpnia", Month.AUGUST);
    MultiteatrEventsMapper.months.put("września", Month.SEPTEMBER);
    MultiteatrEventsMapper.months.put("października", Month.OCTOBER);
    MultiteatrEventsMapper.months.put("listopada", Month.NOVEMBER);
    MultiteatrEventsMapper.months.put("grudnia", Month.DECEMBER);
  }

  public static List<Event> eventsFrom(WebElement spectacleSection) {
    WebElement title = spectacleSection.findElement(By.xpath(".//a"));
    return datesFrom(spectacleSection).stream()
        .map(dateTime -> Event.builder()
            .provider(MULTITEATR)
            .title(title.getText())
            .date(dateTime)
            .city(cityFrom(spectacleSection))
            .place(placeFrom(spectacleSection))
            .descriptionUri(URI.create(title.getAttribute("href")))
            .purchaseUri(URI.create(title.getAttribute("href")))
            .build())
        .collect(toList());
  }

  private static String cityFrom(WebElement spectacleSection) {
    WebElement heading = spectacleSection.findElement(By.xpath("(./h5)[3]"));
    return heading.getText();
  }

  private static String placeFrom(WebElement spectacleSection) {
    WebElement heading = spectacleSection.findElement(By.xpath("(./h5)[2]"));
    return heading.getText();
  }

  private static List<LocalDateTime> datesFrom(WebElement spectacleSection) {
    Select dates = new Select(spectacleSection.findElement(By.xpath(".//select")));
    return dates.getOptions().stream()
        .map(WebElement::getText)
        .map(MultiteatrEventsMapper::toLocalDateTime)
        .collect(toList());
  }

  private static LocalDateTime toLocalDateTime(String text) {
    String[] parts = text.split("[ |]+");
    int dayOfTheMonth = Integer.parseInt(parts[1]);
    Month month = months.get(parts[2]);
    int year = Integer.parseInt(parts[3]);
    String[] time = parts[4].split(":");
    int hour = Integer.parseInt(time[0]);
    int minute = Integer.parseInt(time[1]);
    return LocalDateTime.of(year, month, dayOfTheMonth, hour, minute);
  }
}
