package com.protonmail.mateuszkucharczyk.theatresearch.ebilet;

import com.protonmail.mateuszkucharczyk.theatresearch.driver.WebDriverFactory;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class EBiletSessionFactory {
  private final WebDriverFactory webDriverFactory;

  public EBiletSession make() {
    return new EBiletSession(webDriverFactory.make());
  }
}
