package com.protonmail.mateuszkucharczyk.theatresearch;


import static java.util.Collections.singletonList;

import com.google.common.collect.Range;
import java.time.LocalDateTime;
import java.util.List;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Value;

@Value
@Builder
public class EventCriteria {

  public static EventCriteria alwaysMatch() {
    return EventCriteria.builder()
        .city("")
        .dates(singletonList(Range.all()))
        .build();
  }

  @Default String city = "";
  @Default List<Range<LocalDateTime>> dates = singletonList(Range.all());
}
