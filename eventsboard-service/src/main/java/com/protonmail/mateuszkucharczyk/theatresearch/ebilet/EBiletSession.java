package com.protonmail.mateuszkucharczyk.theatresearch.ebilet;

import java.net.URI;
import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class EBiletSession implements AutoCloseable {

  private final WebDriver webDriver;

  public EBiletSearchPage navigateToSearchPage() {
    return new EBiletSearchPage(webDriver);
  }

  @Override
  public void close() {
    webDriver.quit();
  }

  public EBiletEventDetailsPage navigateToEventDetailsPage(URI uri) {
    return new EBiletEventDetailsPage(webDriver)
        .navigateTo(uri);
  }
}
