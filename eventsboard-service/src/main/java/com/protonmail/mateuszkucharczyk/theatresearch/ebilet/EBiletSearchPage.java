package com.protonmail.mateuszkucharczyk.theatresearch.ebilet;

import static org.openqa.selenium.support.ui.ExpectedConditions.invisibilityOfAllElements;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOfElementLocated;

import com.google.common.collect.Range;
import java.net.URI;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.http.client.utils.URIBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.WebDriverWait;

@Slf4j
class EBiletSearchPage {

  private static final URI EBILET_SEARCH_ENGINE_URI = URI.create("https://www.ebilet.pl/wydarzenia/");

  private final WebDriver webDriver;
  private WebElement eventsElement;
  private List<WebElement> loadersElements;


  public EBiletSearchPage(WebDriver webDriver) {
    this.webDriver = webDriver;
  }

  @SneakyThrows
  public EBiletSearchPage searchBy(EBiletCriteria criteria) {
    log.debug("Navigate to '{}'", EBILET_SEARCH_ENGINE_URI);
    URIBuilder uri = new URIBuilder(EBILET_SEARCH_ENGINE_URI)
        .setFragment("events")
        .addParameter("k", "teatr")
        .addParameter("q", criteria.query());
    if (criteria.timePeriod().hasLowerBound()) {
      uri.addParameter("c", commaSeparatedEpochSecondsFrom(criteria.timePeriod()));
    }

    webDriver.navigate().to(uri.build().toURL());
    eventsElement = waitMaxFor(10).until(visibilityOfElementLocated(By.id("events")));
    loadersElements = webDriver.findElements(By.className("loader-overlay"));
    waitUntilLoaderOverlayInvisible();
    return this;
  }

  private String commaSeparatedEpochSecondsFrom(Range<LocalDateTime> timePeriod) {
    if (!timePeriod.hasLowerBound()) {
      throw new IllegalArgumentException("Time Period does not have starting date");
    }

    StringBuilder result = new StringBuilder();
    result.append(timePeriod.lowerEndpoint().toEpochSecond(ZoneOffset.ofHours(1)));
    if (timePeriod.hasUpperBound()) {
      result.append(',');
      result.append(timePeriod.upperEndpoint().toEpochSecond(ZoneOffset.ofHours(1)));
    }

    return result.toString();
  }

  public List<URI> getLinksToEvents() {
    List<URI> result = new ArrayList<>();

    while (true) {
      eventsElement.findElements(By.xpath(".//a[@class='cube']"))
          .stream()
          .map(element -> URI.create(element.getAttribute("href")))
          .forEach(result::add);
      if (hasNext()) {
        clickNext();
      } else {
        break;
      }
    }
    return result;
  }

  private boolean hasNext() {
    return eventsElement.findElements(By.className("next"))
        .stream()
        .anyMatch(WebElement::isDisplayed);
  }

  private void clickNext() {
    log.debug("Simulate clicking 'Next' button");
    eventsElement.findElements(By.className("next"))
        .stream()
        .filter(WebElement::isDisplayed)
        .findFirst()
        .orElseThrow(() -> new NoSuchElementException("No displayed 'Next' button."))
        .click();
    waitUntilLoaderOverlayInvisible();
  }

  private void waitUntilLoaderOverlayInvisible() {
    waitMaxFor(30).until(invisibilityOfAllElements(loadersElements));
  }

  private WebDriverWait waitMaxFor(int seconds) {
    return new WebDriverWait(webDriver, seconds);
  }
}
