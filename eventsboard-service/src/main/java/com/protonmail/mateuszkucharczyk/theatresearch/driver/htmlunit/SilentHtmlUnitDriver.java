package com.protonmail.mateuszkucharczyk.theatresearch.driver.htmlunit;

import com.gargoylesoftware.htmlunit.SilentCssErrorHandler;
import com.gargoylesoftware.htmlunit.WebClient;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;

class SilentHtmlUnitDriver extends HtmlUnitDriver {
  @Override
  protected WebClient modifyWebClient(WebClient client) {
    WebClient modifiedClient = super.modifyWebClient(client);
    modifiedClient.setCssErrorHandler(new SilentCssErrorHandler());
    modifiedClient.setJavaScriptErrorListener(new SilentJavaScriptErrorListener());
    modifiedClient.setIncorrectnessListener(new SilentIncorrectnessListener());
    modifiedClient.getOptions().setThrowExceptionOnScriptError(false);
    return modifiedClient;
  }
}
