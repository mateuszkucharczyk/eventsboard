package com.protonmail.mateuszkucharczyk.theatresearch;

import static java.time.format.DateTimeFormatter.ISO_LOCAL_DATE;
import static java.time.temporal.ChronoUnit.DAYS;
import static java.util.Arrays.stream;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.toList;

import com.google.common.collect.Range;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;


@Controller
@RequiredArgsConstructor
public class EventsController {

  private static final String PAGE = "events";
  private final EventsRepository eventsRepository;

  @GetMapping("/" + PAGE)
  public String spectacles(
      @RequestParam(name = "city", defaultValue = "Warszawa") String city,
      @RequestParam(name = "dates", required = false, defaultValue = "") String commaSeparatedLocalDates,
      Model model) {

    if (commaSeparatedLocalDates.isEmpty()) {
      commaSeparatedLocalDates = LocalDate.now(ZoneId.of("Europe/Warsaw")).toString();
    }

    model.addAttribute("city", city);
    model.addAttribute("dates", commaSeparatedLocalDates);
    EventCriteria criteria = new EventCriteria(city, listOfLocalDateTimeFrom(commaSeparatedLocalDates));
    model.addAttribute("events", eventsRepository.findBy(criteria));

    return PAGE;
  }

  private static List<Range<LocalDateTime>> listOfLocalDateTimeFrom(String commaSeparatedLocalDates) {
    List<LocalDate> localDates = stream(commaSeparatedLocalDates.split(","))
        .map(String::trim)
        .map(EventsController::toLocalDate)
        .collect(toList());

    return mergeOverlapping(localDates)
        .stream()
        .map(EventsController::toDateTimeRange)
        .collect(toList());
  }

  private static LocalDate toLocalDate(String localDate) {
    return LocalDate.parse(localDate, ISO_LOCAL_DATE);
  }

  private static Range<LocalDateTime> toDateTimeRange(Range<LocalDate> localDate) {
    if (!localDate.hasLowerBound()) {
      throw new IllegalArgumentException(
          "Cannot map " + localDate + " to LocalDateTime because range starts at -Infinity.");
    }

    if (!localDate.hasUpperBound()) {
      throw new IllegalArgumentException(
          "Cannot map " + localDate + " to LocalDateTime because range ends at +Infinity");
    }

    return Range.closed(
        localDate.lowerEndpoint().atStartOfDay(),
        localDate.upperEndpoint().atTime(23, 59, 59));
  }

  private static List<Range<LocalDate>> mergeOverlapping(List<LocalDate> timePeriods) {
    if (timePeriods.isEmpty()) {
      return emptyList();
    }

    Queue<LocalDate> datesToBeMerged = new LinkedList(timePeriods);
    List<Range<LocalDate>> mergedRanges = new ArrayList<>();

    Range<LocalDate> currentRange = Range.singleton(datesToBeMerged.poll());
    while (true){
      LocalDate nextDate = datesToBeMerged.poll();
      if (nextDate == null) {
        mergedRanges.add(currentRange);
        break;
      } else if (currentRange.contains(nextDate.minus(1, DAYS))) {
        currentRange = Range.closed(currentRange.lowerEndpoint(), nextDate);
      } else {
        mergedRanges.add(currentRange);
        currentRange = Range.singleton(nextDate);
      }
    }

    return mergedRanges;
  }

  @EventListener(ApplicationReadyEvent.class)
  public void printSampleUrl() {
    System.out.println("http://localhost:8080/" + PAGE);
  }
}
