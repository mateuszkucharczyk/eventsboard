package com.protonmail.mateuszkucharczyk.theatresearch.ebilet;

import static com.protonmail.mateuszkucharczyk.theatresearch.Predicates.byEventDateOverlapping;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

import com.google.common.collect.Range;
import com.protonmail.mateuszkucharczyk.theatresearch.Event;
import com.protonmail.mateuszkucharczyk.theatresearch.EventCriteria;
import com.protonmail.mateuszkucharczyk.theatresearch.EventsRepository;
import java.net.URI;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import lombok.Cleanup;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@RequiredArgsConstructor
class EBiletRepository implements EventsRepository {

  private final EBiletSessionFactory sessionFactory;

  @Override
  public List<Event> findBy(EventCriteria criteria) {
    List<Event> events = new ArrayList<>();
    for (Range<LocalDateTime> timePeriod : criteria.getDates()) {
      EBiletCriteria eBiletCriteria = EBiletCriteria.builder()
          .query(criteria.getCity())
          .timePeriod(timePeriod)
          .build();
      events.addAll(findBy(eBiletCriteria));
    }

    return deduplicate(events)
        .stream()
        .filter(byCity(criteria.getCity()))
        .filter(byEventDateOverlapping(criteria.getDates()))
        .sorted(comparing(Event::getDate))
        .collect(toList());
  }

  private List<Event> findBy(EBiletCriteria criteria) {
    @Cleanup EBiletSession session = sessionFactory.make();

    List<URI> uris = session.navigateToSearchPage()
        .searchBy(criteria)
        .getLinksToEvents();

    List<Event> events = new ArrayList<>();
    for (URI uri : uris) {
      events.addAll(session
          .navigateToEventDetailsPage(uri)
          .getEvents());
    }
    return events;
  }

  private List<Event> deduplicate(List<Event> events) {
    Map<URI, Event> uniqueEvents = new HashMap<>();
    for (Event event : events) {
      uniqueEvents.put(event.getPurchaseUri(), event);
    }
    return new ArrayList<>(uniqueEvents.values());
  }

  private static Predicate<Event> byCity(String city) {
    return event -> event.getCity().equals(city);
  }

}
