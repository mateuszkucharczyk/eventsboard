package com.protonmail.mateuszkucharczyk.theatresearch.ebilet;

import com.google.common.collect.Range;
import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Value;
import lombok.experimental.Accessors;


@Value
@Builder
@Accessors(fluent = true)
class EBiletCriteria {
  Range<LocalDateTime> timePeriod;
  String query;
}
