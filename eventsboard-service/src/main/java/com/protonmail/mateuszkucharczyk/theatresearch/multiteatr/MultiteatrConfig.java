package com.protonmail.mateuszkucharczyk.theatresearch.multiteatr;

import com.protonmail.mateuszkucharczyk.theatresearch.CachedEvents;
import com.protonmail.mateuszkucharczyk.theatresearch.EventsRepository;
import com.protonmail.mateuszkucharczyk.theatresearch.driver.htmlunit.WebDriverHtmlUnitConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@ConditionalOnProperty(prefix = MultiteatrConfig.PREFIX, name = "enabled", havingValue = "true")
@Configuration
@Import(WebDriverHtmlUnitConfiguration.class)
public class MultiteatrConfig {

  static final String PREFIX = "events.providers.multiteatr";
  private static final String CACHE = PREFIX + ".cache";

  @Bean
  @ConditionalOnProperty(prefix = CACHE, name = "enabled", havingValue = "false", matchIfMissing = true)
  public static EventsRepository mutliteatrRepository(WebDriverHtmlUnitConfiguration config) {
    return new MultiteatrRepository(new MultiteatrSessionFactory(config.htmlUnitWebDriverFactory()));
  }

  @Bean
  @ConditionalOnProperty(prefix = CACHE, name = "enabled", havingValue = "true")
  public static EventsRepository cachedMutliteatrRepository(WebDriverHtmlUnitConfiguration config) {
    return new CachedEvents(mutliteatrRepository(config));
  }
}
