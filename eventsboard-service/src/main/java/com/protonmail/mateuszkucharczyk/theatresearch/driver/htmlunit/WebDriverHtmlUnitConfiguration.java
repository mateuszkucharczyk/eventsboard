package com.protonmail.mateuszkucharczyk.theatresearch.driver.htmlunit;

import com.protonmail.mateuszkucharczyk.theatresearch.driver.SeleniumConfiguration;
import com.protonmail.mateuszkucharczyk.theatresearch.driver.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import(SeleniumConfiguration.class)
@Configuration
public class WebDriverHtmlUnitConfiguration {

  @Bean
  public WebDriverFactory htmlUnitWebDriverFactory() {
    return this::webdriver;
  }

  private WebDriver webdriver() {
    HtmlUnitDriver driver = new SilentHtmlUnitDriver();
    driver.setDownloadImages(false);
    driver.setJavascriptEnabled(true);
    return driver;
  }

}
