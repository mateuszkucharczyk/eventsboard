package com.protonmail.mateuszkucharczyk.theatresearch.multiteatr;

import lombok.RequiredArgsConstructor;
import org.openqa.selenium.WebDriver;

@RequiredArgsConstructor
public class MultiteatrSession implements AutoCloseable {

  private final WebDriver webDriver;

  public MultiteatrSearchPage navigateToSearchPage() {
    return new MultiteatrSearchPage(webDriver).navigate();
  }

  @Override
  public void close() {
    webDriver.quit();
  }
}
