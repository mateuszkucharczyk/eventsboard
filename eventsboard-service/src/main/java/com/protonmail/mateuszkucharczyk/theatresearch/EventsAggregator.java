package com.protonmail.mateuszkucharczyk.theatresearch;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class EventsAggregator implements EventsRepository {

  @NonNull private final List<EventsRepository> repositories;

  @Override
  public List<Event> findBy(@NonNull EventCriteria criteria) {
    List<Event> result = new ArrayList<>();
    for (EventsRepository repository : repositories) {
      result.addAll(repository.findBy(criteria));
    }
    result.sort(Comparator.comparing(Event::getDate));
    return result;
  }
}
