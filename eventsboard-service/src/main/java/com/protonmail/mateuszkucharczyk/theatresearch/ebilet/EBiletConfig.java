package com.protonmail.mateuszkucharczyk.theatresearch.ebilet;

import com.protonmail.mateuszkucharczyk.theatresearch.CachedEvents;
import com.protonmail.mateuszkucharczyk.theatresearch.EventsRepository;
import com.protonmail.mateuszkucharczyk.theatresearch.driver.firefox.WebDriverFirefoxConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@ConditionalOnProperty(prefix = EBiletConfig.PREFIX,  name = "enabled", havingValue = "true")
@Configuration
@Import(WebDriverFirefoxConfiguration.class)
public class EBiletConfig {

  static final String PREFIX = "events.providers.ebilet";
  private static final String CACHE = PREFIX + ".cache";

  @Bean
  @ConditionalOnProperty(prefix = CACHE, name = "enabled", havingValue = "false", matchIfMissing = true)
  public static EventsRepository eBiletRepository(WebDriverFirefoxConfiguration config) {
    return new EBiletRepository(new EBiletSessionFactory(config.firefoxWebDriverFactory()));
  }

  @Bean
  @ConditionalOnProperty(prefix = CACHE, name = "enabled", havingValue = "true")
  public static EventsRepository cachedEBiletRepository(WebDriverFirefoxConfiguration config) {
    return new CachedEvents(eBiletRepository(config));
  }
}
