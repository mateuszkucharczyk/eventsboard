package com.protonmail.mateuszkucharczyk.theatresearch;

import java.net.URI;
import java.time.LocalDateTime;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class Event {
  final String provider;
  final String title;
  final LocalDateTime date;
  final String city;
  final String place;
  final URI descriptionUri;
  final URI purchaseUri;
}
