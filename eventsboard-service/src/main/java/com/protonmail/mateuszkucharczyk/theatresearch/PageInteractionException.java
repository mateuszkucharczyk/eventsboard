package com.protonmail.mateuszkucharczyk.theatresearch;

import static java.lang.String.format;

import java.net.URL;

public class PageInteractionException extends RuntimeException {

  public PageInteractionException(String interaction, URL url, Throwable cause) {
    this(interaction, url.toString(), cause);
  }

  public PageInteractionException(String interaction, String url, Throwable cause) {
    super(format("Failed to %s on page %s", interaction, url), cause);
  }

  public PageInteractionException(String interaction, Throwable cause) {
    super("Failed to " + interaction, cause);
  }
}
