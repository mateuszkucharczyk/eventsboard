package com.protonmail.mateuszkucharczyk.theatresearch.multiteatr;

import static com.protonmail.mateuszkucharczyk.theatresearch.Predicates.byEventDateOverlapping;
import static java.util.Comparator.comparing;
import static java.util.stream.Collectors.toList;

import com.protonmail.mateuszkucharczyk.theatresearch.Event;
import com.protonmail.mateuszkucharczyk.theatresearch.EventCriteria;
import com.protonmail.mateuszkucharczyk.theatresearch.EventsRepository;
import java.util.ArrayList;
import java.util.List;
import lombok.Cleanup;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
class MultiteatrRepository implements EventsRepository {

  private final MultiteatrSessionFactory sessionFactory;

  public List<Event> findBy(EventCriteria criteria) {
    return searchBy(criteria.getCity())
        .stream()
        .filter(byEventDateOverlapping(criteria.getDates()))
        .collect(toList());
  }

  private List<Event> searchBy(String city) {
    @Cleanup MultiteatrSession session = sessionFactory.make();

    MultiteatrSearchPage page = session.navigateToSearchPage();
    page.searchBy(city);
    List<Event> events = new ArrayList<>(page.getSpectacles());
    while (page.hasNextPage()) {
      page.nextPage();
      events.addAll(page.getSpectacles());
    }
    events.sort(comparing(Event::getDate));
    return events;
  }
}
