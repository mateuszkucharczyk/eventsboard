package com.protonmail.mateuszkucharczyk.theatresearch.multiteatr;

import com.protonmail.mateuszkucharczyk.theatresearch.driver.WebDriverFactory;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MultiteatrSessionFactory {
  private final WebDriverFactory webDriverFactory;

  public MultiteatrSession make() {
    return new MultiteatrSession(webDriverFactory.make());
  }
}
