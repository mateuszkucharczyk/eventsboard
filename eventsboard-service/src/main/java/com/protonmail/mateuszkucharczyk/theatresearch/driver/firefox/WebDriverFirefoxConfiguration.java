package com.protonmail.mateuszkucharczyk.theatresearch.driver.firefox;

import static java.lang.Boolean.parseBoolean;
import static java.lang.Integer.parseInt;

import com.protonmail.mateuszkucharczyk.theatresearch.driver.SeleniumConfiguration;
import com.protonmail.mateuszkucharczyk.theatresearch.driver.WebDriverFactory;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import lombok.Getter;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxDriverLogLevel;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Import(SeleniumConfiguration.class)
@Configuration
@ConfigurationProperties(prefix = "firefox")
public class WebDriverFirefoxConfiguration {
  @Getter private final Map<String, String> options = new HashMap<>();
  @Getter private final Map<String, String> profile = new HashMap<>();

  private FirefoxOptions firefoxOptions;

  @Bean
  public WebDriverFactory firefoxWebDriverFactory() {
    this.firefoxOptions = firefoxOptions();
    return () -> new FirefoxDriver(firefoxOptions);
  }

  private FirefoxOptions firefoxOptions() {
    FirefoxOptions options = new FirefoxOptions();
    options.setProfile(profile());
    options.setHeadless(Boolean.parseBoolean(this.options.get("headless")));
    options.setLogLevel(FirefoxDriverLogLevel.ERROR);
    return options;
  }

  private FirefoxProfile profile() {
    FirefoxProfile profile = new FirefoxProfile();
    for (Entry<String, String> entry : this.profile.entrySet()) {
      if (entry.getValue().matches("[0-9]*")) {
        profile.setPreference(entry.getKey(), parseInt(entry.getValue()));
      } else if(entry.getValue().matches("true|false")) {
        profile.setPreference(entry.getKey(), parseBoolean(entry.getValue()));
      } else {
        profile.setPreference(entry.getKey(), entry.getValue());
      }
    }
    return profile;
  }
}
