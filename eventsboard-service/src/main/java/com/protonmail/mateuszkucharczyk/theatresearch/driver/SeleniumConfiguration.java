package com.protonmail.mateuszkucharczyk.theatresearch.driver;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import javax.annotation.PostConstruct;
import lombok.Getter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties
@Getter
public class SeleniumConfiguration {
  private final Map<String, String> selenium = new HashMap<>();

  @PostConstruct
  public void configureSelenium() {
    for (Entry<String, String> entry : selenium.entrySet()) {
      System.setProperty(entry.getKey(), entry.getValue());
    }
  }
}
