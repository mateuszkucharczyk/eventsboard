package com.protonmail.mateuszkucharczyk.theatresearch;

import com.google.common.collect.Range;
import java.time.LocalDateTime;
import java.util.List;
import java.util.function.Predicate;
import lombok.experimental.UtilityClass;

@UtilityClass
public class Predicates {
  public static Predicate<Event> byDateBetween(LocalDateTime from, LocalDateTime till) {
    return spectacle -> between(from, spectacle.getDate(), till);
  }

  private static boolean between(LocalDateTime lowerBound, LocalDateTime dateTime, LocalDateTime upperBound) {
    return (dateTime.equals(lowerBound) || dateTime.isAfter(lowerBound)) &&
        (dateTime.equals(upperBound) || dateTime.isBefore(upperBound));
  }

  public static Predicate<? super Event> byEventDateOverlapping(List<Range<LocalDateTime>> dates) {
    return spectacle -> isOverlapping(dates, spectacle.getDate());
  }

  private static boolean isOverlapping(List<Range<LocalDateTime>> dates, LocalDateTime date) {
    return dates.stream()
        .anyMatch(dateRange -> dateRange.contains(date));
  }
}
