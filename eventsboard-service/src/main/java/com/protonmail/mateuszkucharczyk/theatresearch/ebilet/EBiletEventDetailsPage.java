package com.protonmail.mateuszkucharczyk.theatresearch.ebilet;

import static java.util.stream.Collectors.toList;

import com.protonmail.mateuszkucharczyk.theatresearch.Event;
import java.net.URI;
import java.net.URL;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

@Slf4j
@RequiredArgsConstructor
class EBiletEventDetailsPage implements AutoCloseable {
  private final WebDriver webDriver;

  @Override
  public void close() {
    webDriver.quit();
  }

  public EBiletEventDetailsPage navigateTo(URI uri) {
    log.debug("Navigate to '{}'", uri);
    webDriver.navigate().to(urlFrom(uri));
    return this;
  }

  public List<Event> getEvents() {
    log.debug("Simulate reading event details of {}", webDriver.getCurrentUrl());
    return webDriver.findElements(By.xpath("//a[@class='row performanceItem ']"))
        .stream()
        .map(webElement -> EBiletEventMapper.toSpectacle(webElement, currentUrl()))
        .collect(toList());
  }

  @SneakyThrows
  private URL urlFrom(URI uri) {
      return uri.toURL();
  }

  private URI currentUrl() {
    return URI.create(webDriver.getCurrentUrl());
  }
}
