package com.protonmail.mateuszkucharczyk.theatresearch;

import java.util.List;

public interface EventsRepository {

  List<Event> findBy(EventCriteria criteria);

}
