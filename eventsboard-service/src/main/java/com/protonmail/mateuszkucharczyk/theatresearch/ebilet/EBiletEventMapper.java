package com.protonmail.mateuszkucharczyk.theatresearch.ebilet;

import com.protonmail.mateuszkucharczyk.theatresearch.Event;
import java.net.URI;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

@UtilityClass
@Slf4j
class EBiletEventMapper {

  private static final String E_BILET = "EBilet";
  private static final DateTimeFormatter DATE_TIME_FORMAT = DateTimeFormatter.ofPattern("dd.MM.yyyyHH:mm");
  private static final String UNKNOWN = "Unknown";

  public static Event toSpectacle(WebElement webElement, URI descriptionUri) {
    return Event.builder()
        .provider(E_BILET)
        .title(titleFrom(webElement))
        .descriptionUri(descriptionUri)
        .purchaseUri(buyTicketLinkFrom(webElement))
        .city(cityFrom(webElement))
        .place(placeFrom(webElement))
        .date(dateTimeFrom(webElement))
        .build();
  }

  private static String titleFrom(WebElement webElement) {
    try {
      return findHiddenTextOf(webElement, By.className("item-title"));
    } catch (Exception e) {
      logFailedToParse("title", e);
      return UNKNOWN;
    }
  }

  private static URI buyTicketLinkFrom(WebElement webElement) {
    try {
      return URI.create(webElement.getAttribute("href"));
    } catch (Exception e) {
      logFailedToParse("href", e);
      return URI.create("#");
    }
  }

  private static String cityFrom(WebElement webElement) {
    try {
      return findHiddenTextOf(webElement, By.className("city-header"));
    } catch (Exception e) {
      logFailedToParse("city", e);
      return UNKNOWN;
    }
  }

  private static String placeFrom(WebElement webElement) {
    try {
      return descriptionFrom(webElement)[1];
    } catch (Exception e) {
      logFailedToParse("place", e);
      return UNKNOWN;
    }
  }

  private static LocalDateTime dateTimeFrom(WebElement webElement) {
    try {
      return LocalDateTime.parse(dateFrom(webElement) + timeFrom(webElement), DATE_TIME_FORMAT);
    } catch (Exception e) {
      logFailedToParse("datetime", e);
      return LocalDateTime.MIN;
    }
  }

  private static String timeFrom(WebElement webElement) {
    try {
      return descriptionFrom(webElement)[0].split(",\\s")[1];
    } catch (Exception e) {
      logFailedToParse("time", e);
      return "00:00";
    }
  }

  private static String[] descriptionFrom(WebElement webElement) {
    return findHiddenTextOf(webElement, By.className("description-inline-container"))
        .split("\\n");
  }

  private static String dateFrom(WebElement webElement) {
    try {
      return findHiddenTextOf(webElement, By.className("item-date")) + ".2020";
    } catch (Exception e) {
      logFailedToParse("date", e);
      return "01.01.1970";
    }
  }

  private static String findHiddenTextOf(WebElement webElement, By by) {
    return webElement.findElement(by).getAttribute("textContent").trim();
  }

  private static void logFailedToParse(String field, Exception cause) {
    log.error("Failed to parse event {} due to {}", field, cause.getMessage());
  }
}
