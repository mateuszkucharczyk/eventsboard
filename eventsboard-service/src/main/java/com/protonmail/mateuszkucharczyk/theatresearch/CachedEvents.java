package com.protonmail.mateuszkucharczyk.theatresearch;

import static com.protonmail.mateuszkucharczyk.theatresearch.EventCriteria.alwaysMatch;
import static com.protonmail.mateuszkucharczyk.theatresearch.Predicates.byEventDateOverlapping;
import static java.util.Collections.emptyList;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.toList;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Scheduled;

@Slf4j
@RequiredArgsConstructor
public class CachedEvents implements EventsRepository {

  private static final int THIRTY_MINUTES = 30 * 60 * 1000;

  private final EventsRepository spectaclesService;
  private Map<String, List<Event>> cachedSpectaclesByCity = new ConcurrentHashMap<>();

  @Scheduled(fixedDelay = THIRTY_MINUTES)
  public void refreshSpectaclesCache() {
    log.info("Cache refreshing started.");
    List<Event> events = spectaclesService.findBy(alwaysMatch());
    cachedSpectaclesByCity.clear();
    cachedSpectaclesByCity.putAll(groupedByCity(events));
    log.info("Cache refreshing finished.");
  }

  private static Map<String, List<Event>> groupedByCity(List<Event> events) {
    return events.stream().collect(groupingBy(Event::getCity));
  }

  @Override
  public List<Event> findBy(EventCriteria criteria) {
      return getEventsByCity(criteria.getCity())
          .filter(byEventDateOverlapping(criteria.getDates()))
          .collect(toList());

  }

  private Stream<Event> getEventsByCity(String city) {
    if (city.isEmpty()) {
      return cachedSpectaclesByCity.values()
          .stream()
          .flatMap(Collection::stream);
    } else {
      return cachedSpectaclesByCity.getOrDefault(city, emptyList())
          .stream();
    }
  }
}
