package com.protonmail.mateuszkucharczyk.theatresearch.multiteatr;

import static java.util.stream.Collectors.toList;

import com.protonmail.mateuszkucharczyk.theatresearch.Event;
import com.protonmail.mateuszkucharczyk.theatresearch.PageInteractionException;
import java.util.List;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

@Slf4j
@RequiredArgsConstructor
class MultiteatrSearchPage {

  private static final String SPECTACLES_URL = "https://www.multiteatr.pl/Spektakle";
  private static final String NEXT_BUTTON_ID = "body_lbPageBottomNext";
  public static final String CITY_ALWAYS_MATCH = "MIASTO";

  @NonNull private final WebDriver web;

  public MultiteatrSearchPage navigate() {
    web.navigate().to(SPECTACLES_URL);
    return this;
  }

  public MultiteatrSearchPage searchBy(String city) {
    if (city.isEmpty()) {
      chooseCity(CITY_ALWAYS_MATCH);
    }
    return clickSearchButton();
  }

  public List<Event> getSpectacles() {
    List<WebElement> spectaclesBoxes = web.findElements(By.xpath("//div[@class='show-box']/div/div[contains(@class,'show-list')]"));
    return spectaclesBoxes.stream()
        .flatMap(htmlDivision -> MultiteatrEventsMapper.eventsFrom(htmlDivision).stream())
        .peek(event -> log.debug("Simulate reading {}", event))
        .collect(toList());
  }

  public boolean hasNextPage() {
    try {
      web.findElement(By.id(NEXT_BUTTON_ID));
      return true;
    } catch (NoSuchElementException e) {
      return false;
    } catch (Exception e) {
      throw new PageInteractionException("check whether Next Button exists", web.getCurrentUrl(), e);
    }
  }

  public MultiteatrSearchPage nextPage() {
    log.debug("Simulate clicking 'Next' button");
    try {
      web.findElement(By.id(NEXT_BUTTON_ID)).click();
      return this;
    } catch (Exception e) {
      throw new PageInteractionException("click Next Button", web.getCurrentUrl(), e);
    }
  }

  private MultiteatrSearchPage chooseCity(String city) {
    log.debug("Simulate selecting city '{}' from drop-down list", city);
    try {
      Select cities = new Select(web.findElement(By.id("body_ddlCities")));
      cities.selectByVisibleText(city);
      return this;
    } catch (Exception e) {
      throw new PageInteractionException("choose city '" + city + "' from dropdown. Maybe there is no such option? ", web.getCurrentUrl(), e);
    }
  }

  private MultiteatrSearchPage clickSearchButton() {
    log.debug("Simulate clicking 'Search' button");
    try {
      WebElement submitButton = web.findElement(By.id("body_btnSearch"));
      submitButton.click();
      return this;
    } catch (Exception e) {
      throw new PageInteractionException("click Search Button", web.getCurrentUrl(), e);
    }
  }
}
