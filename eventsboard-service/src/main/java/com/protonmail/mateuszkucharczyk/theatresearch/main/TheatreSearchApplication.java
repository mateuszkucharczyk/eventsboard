package com.protonmail.mateuszkucharczyk.theatresearch.main;

import com.protonmail.mateuszkucharczyk.theatresearch.EventsAggregator;
import com.protonmail.mateuszkucharczyk.theatresearch.EventsController;
import com.protonmail.mateuszkucharczyk.theatresearch.EventsRepository;
import com.protonmail.mateuszkucharczyk.theatresearch.ebilet.EBiletConfig;
import com.protonmail.mateuszkucharczyk.theatresearch.multiteatr.MultiteatrConfig;
import java.util.List;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
@Import({MultiteatrConfig.class, EBiletConfig.class})
@RequiredArgsConstructor
public class TheatreSearchApplication {

  @Bean
  private static EventsController eventsController(List<EventsRepository> repositories) {
    return new EventsController(new EventsAggregator(repositories));
  }

  public static void main(String[] args) {
    new SpringApplicationBuilder(TheatreSearchApplication.class)
        .run(args);
  }
}
