package com.protonmail.mateuszkucharczyk.theatresearch.driver;

import org.openqa.selenium.WebDriver;

public interface WebDriverFactory {
  WebDriver make();
}
